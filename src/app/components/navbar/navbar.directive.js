/**
 * Created by alex on 7/15/2016.
 */
(function() {
    'use strict';

    angular
        .module('app')
        .directive('ngcNavbar', ngcNavbar);

    /** @ngInject */
    function ngcNavbar() {
        return  {
            restrict: 'EA',
            templateUrl: 'app/components/navbar/navbar.html',
            scope: {
                data: '='
            },
            controller: NavbarController,
            controllerAs: 'navCtrl',
            bindToController: true
        };


        /** @ngInject */
        function NavbarController($auth, Auth, $state, $log) {
            var vm = this;
            $log.info(vm.data);
            vm.isAuthenticated = $auth.isAuthenticated();

            vm.signIn = function(){
                return login().then(function(){
                    vm.isAuthenticated = true;
                    $state.go('services');
                }).catch(function(response) {
                    vm.authError = response.data.message;
                });
            };

            vm.logout = function (){
                $auth.logout();
                $state.go('main');
                vm.isAuthenticated = false;
            };

            function login() {
                return $auth.login(vm.user).then(getUser);
            }

            function getUser() {
                return  Auth.getUser().then(function(response) {
                    vm.user = response;
                    return vm.user;
                });
            }

            if(vm.isAuthenticated ){
                getUser();
            }
        }
    }

})();
