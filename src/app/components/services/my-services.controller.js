/**
 * Created by alex on 7/15/2016.
 */
(function(){
    'use strict';
    angular.module('app')
        .controller('MyServicesController', MyServicesController);

    function MyServicesController(){
        var vm = this;
        vm.title = 'My Services'
    }
})();