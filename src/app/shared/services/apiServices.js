(function() {
    'use strict';
    angular.module('app')
        .factory('apiServices', apiServices);

    function apiServices($http, $q) {
        var base_url = '/api';
        return {
            get :get,
            post: post,
            put: put,
            del:del,
            profile: profile,
            userLogged: userLogged
        };


        function get (url, params) {
            return $http.get(base_url + url, {params: params}).
                then(function (response) {
                    return response.data;
                },function (errResponse) {
                    return $q.reject(errResponse);
                });
        }

        function post (url, payload) {
            return $http.post(base_url + url, payload).
                then(function (response) {
                    return response.data;
                }, function(errResponse) {
                return $q.reject(errResponse);
            });
        }

        function put (url, itemId, payload) {
            return $http.put(base_url + url +'/'+ itemId, payload).
                then(function (response) {
                    return response.data;
                }, function (errResponse) {
                    return $q.reject(errResponse);
                });
        }

        function del (url, itemId) {
            return $http.delete(base_url + url + '/'+ itemId).
                then(function (response) {
                    return response.data;
                },function(errResponse) {
                    return $q.reject(errResponse);
                });
        }

         function profile (userId){
            return $http.get(base_url + '/users/' + userId + '/profiles/' + userId).then(function (response) {
                return response.data;
            },function (errResponse) {
                return $q.reject(errResponse);
            });
        }

        function userLogged(){
            return $http.get(base_url + '/user/logged').then(function (response) {
                return response.data;
            },function (errResponse) {
                return $q.reject(errResponse);
            });
        }
    }
})();

