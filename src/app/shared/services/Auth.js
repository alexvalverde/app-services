(function() {
    'use strict';
    angular.module('app')
        .factory('Auth', Auth);

    function Auth($q, apiServices) {

        var vm = this;
        return {
            getUser: getUser
        };

        function getUser( ) {
            var defered = $q.defer();
            var promise = defered.promise;
            if(vm.user){
                //vm.user = $localStorage.user;
                defered.resolve(vm.user);
                return promise
            }else {
                return apiServices.userLogged()
                    .then(function (response){
                        vm.user = response.data;
                        return  vm.user;
                    });
            }
        }
    }
})();

