(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
      $stateProvider
          .state('main', {
              url: '/main?page?name?category_id?description?min?max',
              templateUrl: 'app/main/main.html',
              controller: 'MainController',
              controllerAs: 'mainCtrl'
          })
          .state('profile', {
              url: '/profile',
              templateUrl: 'app/components/profile/profile.html',
              controller: 'ProfileController',
              controllerAs: 'profileCtrl'
          })
          .state('services', {
              url: '/my-services',
              templateUrl: 'app/components/services/services.html',
              controller: 'MyServicesController',
              controllerAs: 'msCtrl'
          });
      $urlRouterProvider.otherwise( '/main' );
  }

})();
