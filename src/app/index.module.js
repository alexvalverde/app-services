(function() {
    'use strict';

    angular
        .module('app', [
            'ngAnimate',
            'ngTouch',
            'ngSanitize',
            'ngMessages',
            'ngAria',
            'ui.router',
            'ui.bootstrap',
            'toastr',
            'satellizer',
            'ngStorage'
        ]);
})();
