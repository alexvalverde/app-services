/**
 * Created by Alexander on 15-06-2016.
 */
(function(){
    'use strict';
    angular.module('app')
        .controller('MainController', MainController);

    function MainController($http, $state, $filter){
        var vm = this;
        vm.title = 'Services';
        vm.numPages = 10;

        vm.params = {
            page:  $state.params.page ? $state.params.page : 1,
            category_id: $state.params.category_id ? $state.params.category_id : null,
            name: $state.params.name ? $state.params.name : null,
            min: $state.params.min ? parseFloat($state.params.min) : null,
            max: $state.params.max ? parseFloat($state.params.max) : null,
            pageSize: 12,
            sort: {
                property : 'created_at',
                dir: 'ASC'
            }
        };

        vm.change = function(){
            console.log(vm.params.name);
        };

        vm.load = function() {
            return getCategories()
                .then(function(){
                   return getServices();
                })
                .then(function() {
                    vm.loaded = true;
                })
                .catch(function(resposne) {
                    console.log(response)
                });
        };

        function getCategories(){
            return $http.get('/api/categories').then(function (response){
                vm.categories = response.data.data;
                vm.categories.unshift({name: 'Choose Category'});
                if($state.params.category_id){
                    vm.selectedCategory = $filter('filter')(vm.categories, {id :$state.params.category_id });
                    vm.selectedCategory =   vm.selectedCategory[0];
                }else {
                    vm.selectedCategory = vm.categories[0];
                }
            });
        }

        function getServices() {
            return $http.get('/api/services', { params: vm.params }).then(function (response){
                vm.services = response.data.data;
                vm.totalItems = response.data.total;
                vm.page = response.data.current_page;
                angular.forEach( vm.services , function( value ){
                    value.updated_at =  Date.parse(value.updated_at)
                });
                return vm.services;
            })
        }

        vm.pageChanged = function () {
            $state.go($state.$current.name, {page: vm.params.page});
        };

        vm.reset = function () {
            $state.current.params = { };
            vm.params = {};
            window.location = '/#/main'
        };
        vm.filter = function () {
            vm.params.category_id = vm.selectedCategory.id ? vm.selectedCategory.id : null;
            $state.go($state.$current.name, vm.params);
            vm.load();
        };

        vm.load();
    }

})();